# 4D Miner Translations
A Git repo for managing all language translations in 4D Miner.

The goal of this project is to make it as easy as possible for members of the 4D Miner community to collaborate on translations. Rather than relying on translation tools like Google Translate or ChatGPT, which have their own limitations, this project aims to allow actual people, many of whom have no other way to support 4D Miner's development, to provide the best possible translations for the game. Automatic translation tools are much more likely to miss context and nuances when translating, especially for a game like 4D Miner, where community members are going to be much more knowledgeable about specific details.

## Contributing
Many people don't know how to work on Git repos, so a brief explanation will be provided here (it's actually pretty simple).

### Forking
Only users with special privileges are able to directly make changes to this project. It isn't feasible to allow everybody to directly contribute, because bad actors could make harmful changes if left unchecked, and even well-intentioned users who don't know what they're doing could accidentally mess things up. As a result, Git project maintainers usually expect most users to create what's called a "fork" of their project in order to contribute.

A "fork" is just a copy of the original project, and anyone who creates a fork is completely in charge of it. After making the necessary changes to the fork, a merge request (aka pull request) can be made to the original project. The maintainers who are in charge of the original project can then approve the merge request, and all of the changes made to the fork will be applied to the original project.

A new fork doesn't need to be made every time a user wants to make a contribution; an existing fork can be reused, but if any changes have been made to the original project since the last merge request was accepted, it is usually a good idea to sync the fork with the original project before creating another merge request.

### Committing
Every time a change is made to either a fork or to the original project, a commit message is required. Users will automatically be prompted to add a commit message before applying changes to their own fork, and it is important to make the message descriptive. When a merge request is eventually made, the project maintainers will be able to see all of the commit messages, and they probably won't be willing to accept the merge request if they don't understand what is being changed, and why the changes are being made.

### Language Files
Each language is assigned its own language file, where all of the translations for that language will be stored. All of the language files in this project can be found in the ["src" directory](https://gitlab.com/Mashpoe/4dm-translations/-/tree/main/src?ref_type=heads).

Language files are written in the JSON format, which is relatively easy to use without any prior knowledge. Text editors like VS Code or the GitLab Web IDE will make it easier to spot errors when editing.

All language files must be named according to the [ISO 639 two-letter language codes](https://en.wikipedia.org/wiki/List_of_ISO_639_language_codes), e.g. `en.json` for English or `es.json` for Spanish. If a language isn't assigned a two-letter code, a three-letter code from the [official ISO 639-3 website](https://iso639-3.sil.org/code_tables/639/data) can be used instead, e.g. `tok.json` for Toki Pona.

A new language file can easily be created by copying the current `en.json` file and renaming it accordingly.

Each translation has a **title** and a **value**. The title of each translation **must** match the corresponding title in the `en.json` file, so if a new language file is created from a copy of `en.json`, the titles of the translations must not be changed.

Each translation looks something like this:

`en.json`:
```json
"TranslationTitle": "This is the translation content, which is the part that can be modified."
```

`es.json`:
```json
"TranslationTitle": "Este es el contenido de la traducción, que es la parte que se puede modificar."
```

The first translation in each language file is titled "LanguageName", and its value must be the name of the language **in that language**.

### Checking for Errors
There are various types of errors that can be found in language files.

The simplest errors can be easily found on [this repo's webpage](https://mashpoe.gitlab.io/4dm-translations), which lists all JSON errors, missing translations, unused translations, and all translations that match their English counterparts exactly.

JSON errors are the most critical, because they will prevent all of the translations in a given language file from being loaded in 4D Miner.

Missing translations are translations that don't exist in a given language file, but do exist in the English file, `en.json`. If a translation is missing from a language file, the English version will be loaded instead. This functionality must not be relied upon, even though some translations might be identical to their English counterparts.

Unused translations are translations that appear in a given language file, but don't appear in `en.json`. These are pieces of text that have either been removed from 4D Miner, or have been moved to a different location. In the latter case, there will probably be a translation with a similar title in the "Missing Translations" section of the project webpage.

Exact matches are translations that match their English counterparts exactly. These aren't necessarily incorrect, because many languages have words and sometimes even phrases that are written exactly the same in English, but they are listed on the project webpage in order to make it easier to find translations that have been copied from `en.json` and haven't been updated yet.

The webpage for a fork of this project will still only display errors for the original project. In order to check for errors in a fork of this project, the GitLab project ID number of the fork must be entered into the text input on the project webpage. This can be done on both this project's webpage and on the fork's webpage.
